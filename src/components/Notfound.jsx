import React from 'react';

function Notfound() {
  return (
    <div className="not-found">
      <h1>404 not found</h1>
    </div>
  );
}

export default Notfound;
