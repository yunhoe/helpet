import './SearchForm.css';
import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import { PropTypes } from 'prop-types';

function SearchForm({ onClick, onFocusInput }) {
  return (
    <div className="search-form">
      <input
        type="text"
        placeholder="궁금한게 있나요? 검색해주세요"
        onFocus={onFocusInput}
      />
      <button type="button" onClick={onClick}>
        <SearchIcon />
      </button>
    </div>
  );
}

SearchForm.propTypes = {
  onClick: PropTypes.func,
  onFocusInput: PropTypes.func,
};

SearchForm.defaultProps = {
  onClick: () => {},
  onFocusInput: () => {},
};

export default SearchForm;
