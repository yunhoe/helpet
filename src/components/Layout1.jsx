import './Layout1.css';
import React from 'react';
import PropTypes from 'prop-types';

function Layout1({ mainContent, sideContent }) {
  return (
    <div className="layout1">
      <div className="layout1__main">{mainContent}</div>

      <div className="layout1__side">{sideContent}</div>
    </div>
  );
}

Layout1.propTypes = {
  mainContent: PropTypes.string,
  sideContent: PropTypes.string,
};

Layout1.defaultProps = {
  mainContent: '',
  sideContent: '',
};

export default Layout1;
